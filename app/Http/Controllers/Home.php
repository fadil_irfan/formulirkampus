<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use Exception;
use App\Models\Diabetes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Home extends Controller
{
    public function index()
    {
        $unitkerja = DB::table('pe3_fakultas')
            ->orderby('urut')
            ->get();
        return view('dashboard',[
            'unitkerja'=>$unitkerja
        ]);
    }

    

    public function hapusdata($id)
    {
        $data=DB::table('formulirs')->where('id', $id)->first();
        $file_pathNpwp = public_path('storage/'.$data->pathNpwp);
        unlink($file_pathNpwp);
        $file_pathKtp = public_path('storage/'.$data->pathKtp);
        unlink($file_pathKtp);
        $file_pathKk = public_path('storage/'.$data->pathKk);
        unlink($file_pathKk);
        DB::table('formulirs')->where('id', $id)->delete();
        return response()->json(['success'=>'Data Formulir dengan id='.$id.' Berhasil Dihapus !']);

    }

    public function listformulir(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('formulirs')
            ->select('formulirs.*')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'nik'=>$item->nik,
                        'nama'=>$item->nama,
                        'jk'=>$item->jk,
                        'status'=>$item->status,
                        'jumlahanak'=>$item->jumlahanak,
                        'unitkerja'=>ResponseFormatter::getUnit($item->unitkerja),
                        'statuspegawai'=>$item->statuspegawai,
                        'pathNpwp'=>$item->pathNpwp,
                        'pathKk'=>$item->pathKk,
                        'pathKtp'=>$item->pathKtp,

                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        return view('formulir.index');
    }

    public function editformulir($id)
    {
        $data=DB::table('formulirs')->where('id', $id)->first();
        $unitkerja = DB::table('pe3_fakultas')
            ->orderby('urut')
            ->get();
        return view('formulir.form',[
            'data'=>$data,
            'unitkerja'=>$unitkerja
        ]);
    }

    public function updateformulir(Request $request)
    {
        try {
            $request->validate([
                'nik'=>'required',
                'nama'=>'required',
                'jk'=>'required',
                'status'=>'required',
                'jumlahanak'=>'required',
                'unitkerja'=>'required',
                'statuspegawai'=>'required'
                
            ]);
            DB::table('formulirs')
                    ->where('id', $request->id)
                    ->update([
                        'nik'=>$request->nik,
                        'nama'=>$request->nama,
                        'jk'=>$request->jk,
                        'status'=>$request->status,
                        'jumlahanak'=>$request->jumlahanak,
                        'unitkerja'=>$request->unitkerja,
                        'prodi'=>'-',
                        'statuspegawai'=>$request->statuspegawai,
                        'updated_at'=>Carbon::now()
                    ]);
            return response()->json(['status'=>'200','success'=>'Data Formulir berhasil diupdate']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }
}
