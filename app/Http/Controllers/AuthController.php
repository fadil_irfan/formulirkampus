<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Helpers\ResponseFormatter;
use App\Models\User;


class AuthController extends Controller
{
    public function index()
    {
        return view('auth.index');
    }

    public function selesai()
    {
        return view('auth.selesai');
    }

    public function formregister()
    {
        return view('auth.register');
    }
    

    

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect('/')->with('success', 'Anda Berhasil Logout ..');
    }

    public function authenticate(Request $request)
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $credentials = request(['email', 'password']);
            if (Auth::attempt($credentials)) {
                if (auth()->user()->roles=='Administrator')
                {
                    return redirect()->intended('/dashboard')->with('warning', 'Hak Akses Sebagai Admin ...');
                }else
                {
                    return redirect()->intended('/masuk')->with('error', 'Role Tidak Terdaftar..');
                }
                
            }else
            {
                return redirect('/masuk')->with('error', 'User Tidak Dikenali..');  
            }
        } catch (Exception $error) {
            return redirect('/masuk')->with('error', 'Terdapat Kesalahan'); 
        }

    }
    public function register(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'max:8'],
            ]);

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'roles' => $request->roles,
            ]);

            $user = User::where('email', $request->email)->first();

            $tokenResult = $user->createToken('authToken')->plainTextToken;

            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ],'User Registered'); 
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ],'Authentication Failed', 500);
        }
    }

    public function formulir()
    {
        $unitkerja = DB::table('pe3_fakultas')
            ->orderby('urut')
            ->get();
        return view('auth.formulir',[
            'unitkerja'=>$unitkerja
        ]);
    }

    public function simpanformulir(Request $request)
    {
        try {
            $request->validate([
                'nik'=>'required',
                'nama'=>'required',
                'jk'=>'required',
                'status'=>'required',
                'jumlahanak'=>'required',
                'unitkerja'=>'required',
                'statuspegawai'=>'required',
                'pathNpwp'=>'image|max:12048',
                'pathKtp'=>'required|image|max:12048',
                'pathKk'=>'required|image|max:12048'
                
            ]);
            if (!is_null($request->pathNpwp))
            {
                $fileNpwp = $request->pathNpwp->store('assets/npwp', 'public');
            }else
            {
                $fileNpwp="assets/npwp/no_image.jpg";
            }
            
            $fileKtp = $request->pathKtp->store('assets/ktp', 'public');
            $fileKk = $request->pathKk->store('assets/kk', 'public');
            $data=DB::table('formulirs')->insert(
                [
                    'id'=>Uuid::uuid4()->toString(),
                    'nik'=>$request->nik,
                    'nama'=>$request->nama,
                    'jk'=>$request->jk,
                    'status'=>$request->status,
                    'jumlahanak'=>$request->jumlahanak,
                    'unitkerja'=>$request->unitkerja,
                    'prodi'=>'-',
                    'statuspegawai'=>$request->statuspegawai,
                    'pathNpwp'=>$fileNpwp,
                    'pathKtp'=>$fileKtp,
                    'pathKk'=>$fileKk,
                    'created_at'=>Carbon::now()
                ]
            );
            

            return response()->json(['status'=>'200','success'=>'Data Formulir berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }
}
