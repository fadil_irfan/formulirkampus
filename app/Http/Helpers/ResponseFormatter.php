<?php

namespace App\Helpers;

use App\Models\Diabetes;
use App\Models\User;
use Illuminate\Support\Facades\DB;

/**
 * Format response.
 */
class ResponseFormatter
{
    /**
     * API Response
     *
     * @var array
     */
    protected static $response = [
        'meta' => [
            'code' => 200,
            'status' => 'success',
            'message' => null,
        ],
        'data' => null,
    ];

    /**
     * Give success response.
     */
    public static function success($data = null, $message = null)
    {
        self::$response['meta']['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response, self::$response['meta']['code']);
    }

    /**
     * Give error response.
     */
    public static function error($data = null, $message = null, $code = 400)
    {
        self::$response['meta']['status'] = 'error';
        self::$response['meta']['code'] = $code;
        self::$response['meta']['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response, self::$response['meta']['code']);
    }

    public static function getUnit($id)
    {
        $getData=DB::table('pe3_fakultas')
                    ->select('pe3_fakultas.*')
                    ->where('kode_fakultas',$id)
                    ->first();
        return $getData->nama_fakultas;
            
        
    }
    public static function getStatus($status,$unit)
    {
        $getData=DB::table('formulirs')
                    ->select('formulirs.*')
                    ->where('statuspegawai',$status)
                    ->where('unitkerja',$unit)
                    ->count();
        return $getData;        
    }

    public static function getStatusJumlah($unit)
    {
        $getData=DB::table('formulirs')
                    ->select('formulirs.*')
                    ->where('unitkerja',$unit)
                    ->count();
        return $getData;        
    }

    public static function getStatusTotal($unit)
    {
        $getData=DB::table('formulirs')
                    ->select('formulirs.*')
                    ->where('statuspegawai',$unit)
                    ->count();
        return $getData;        
    }
    public static function getStatusTot()
    {
        $getData=DB::table('formulirs')
                    ->select('formulirs.*')
                    ->count();
        return $getData;        
    }
}
