<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Home;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* 
Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/masuk', [AuthController::class, 'index'])->name('auth.index');
Route::post('/proses-login', [AuthController::class, 'authenticate'])->name('login.operator');
Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');

Route::get('/register', [AuthController::class, 'formregister'])->name('auth.formregister');
Route::post('/simpanpengguna', [AuthController::class, 'register'])->name('auth.register');

Route::get('/selesai', [AuthController::class, 'selesai'])->name('auth.selesai');
Route::get('/', [AuthController::class, 'formulir'])->name('auth.formulir');
Route::post('/simpanformulir', [AuthController::class, 'simpanformulir'])->name('auth.simpanformulir');

Route::get('/dashboard', [Home::class, 'index'])->name('home.index');


Route::get('/listformulir', [Home::class, 'listformulir'])->name('home.listformulir');
Route::get('/editformulir/{id}', [Home::class, 'editformulir'])->name('home.editformulir');

Route::post('/updateformulir', [Home::class, 'updateformulir'])->name('home.updateformulir');

Route::delete('/hapusformulir/{id}', [Home::class, 'hapusformulir'])->name('home.hapusformulir');



