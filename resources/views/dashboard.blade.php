@extends('layouts.main')


@section('title', 'Dashboard Formulir')


@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
  <div class="row">
    <div class="col-lg-8 mb-4 order-0">
      <div class="card">
        <div class="d-flex align-items-end row">
          <div class="col-sm-7">
            <div class="card-body">
              <h5 class="card-title text-primary">Selamat Datang, {{ auth()->user()->name }}</h5>
              <p class="mb-4">
               
              </p>

              
            </div>
          </div>
          <div class="col-sm-5 text-center text-sm-left">
            <div class="card-body pb-0 px-0 px-md-4">
              <img
                src="../assets/img/illustrations/man-with-laptop-light.png"
                height="140"
                alt="View Badge User"
                data-app-dark-img="illustrations/man-with-laptop-dark.png"
                data-app-light-img="illustrations/man-with-laptop-light.png" />
            </div>
          </div>
        </div>
      </div>
    </div>
    
    
   
  </div>
  <div class="row">
    <div class="col-lg-12 mb-4 order-0">
      <div class="card">
        <div class="d-flex align-items-end row">
          <div class="col-sm-12">
            <div class="card-body">
              <h5 class="card-title text-primary">Rekap Pengumpulan Formulir</h5>
              <p class="mb-4">
               
              </p>
              <table id="refTabel" class="expandable-table" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Unit Kerja</th>
                        <th>Tenaga Kependidikan </th>
                        <th>Dosen Tetap</th>
                        <th>Dosen Luar Biasa</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($unitkerja as $l)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td><b>{{ $l->nama_fakultas }}</b></td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatus('Tenaga Kependidikan',$l->kode_fakultas)}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatus('Dosen Tetap',$l->kode_fakultas)}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatus('Dosen Luar Biasa',$l->kode_fakultas)}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatusJumlah($l->kode_fakultas)}}</td>
                  </tr> 
                          
                  @endforeach
                  <tr>
                    <td colspan="2">Jumlah </td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatusTotal('Tenaga Kependidikan')}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatusTotal('Dosen Tetap')}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatusTotal('Dosen Luar Biasa')}}</td>
                    <td class="text-center">{{ \App\Helpers\ResponseFormatter::getStatusTot()}}</td>
                  </tr> 
                </tbody>
            </table>

              
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
    
   
  </div>
  
</div>
<!-- / Content -->

@endsection



