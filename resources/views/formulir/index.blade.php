@extends('layouts.main')

@section('title', 'Data Formulir')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
<h4 class="py-3 mb-2"><span class="text-muted fw-light">Data Formulir</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
  <div class="row card-header">
    {{-- <div class="col-xl">
        <a href="{{ url('formluka') }}" class="btn btn-primary">Tambah</a>
    </div>
     --}}
  </div>
  
  
  <div class="table-responsive text-nowrap">
    <div class="row card-header">
        <div class="col-xl">
            <table id="refTabel" class="expandable-table" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIK</th>
                        <th>Nama </th>
                        <th>Jenis Kelamin</th>
                        <th>Unit Kerja</th>
                        <th>Status Pegawai</th>
                        <th>Status Pernikahan</th>
                        <th>Jumlah Anak</th>
                        <th>NPWP</th>
                        <th>KTP</th>
                        <th>KK</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    
      
   
  </div>
</div>
<!--/ Basic Bootstrap Table -->

</div>
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
   

    $(document).ready(function() {
        $.noConflict();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

        let table = $('#refTabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: 'listformulir'
            },
            columns: [
                { data: null, orderable: false, searchable: false },
                { data: 'nik' },
                { data: 'nama' },
                { data: 'jk' },
                { data: 'unitkerja' },
                { data: 'statuspegawai' },
                { data: 'status' },
                { data: 'jumlahanak' },
                {
                    data: 'pathNpwp', searchable: false,
                    render: function (data, type, row, meta) {
                        return `<a class="btn btn-outline-primary" target="_blank" href="/storage/${row['pathNpwp']}">Lihat</a>`;
                    },
                    className: 'text-center'
                },
                {
                    data: 'pathKtp', searchable: false,
                    render: function (data, type, row, meta) {
                        return `<a class="btn btn-outline-primary" target="_blank" href="/storage/${row['pathKtp']}">Lihat</a>`;
                    },
                    className: 'text-center'
                },
                {
                    data: 'pathKk', searchable: false,
                    render: function (data, type, row, meta) {
                        return `<a class="btn btn-outline-primary" target="_blank" href="/storage/${row['pathKk']}">Lihat</a>`;
                    },
                    className: 'text-center'
                },
                { data: 'id' }
            ],
            'columnDefs': [
                {
                    "targets": 11,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                        $updateButton = "<a href='editformulir/"+ data +"' class='btn btn-sm btn-info'  >Edit</a>";
                            return $updateButton + "<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-delete' data-id='" + data + "'' data-original-title='Delete' class='btn btn-sm btn-danger deleteUser' >Hapus</a>";
                    }
                }
            ]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i+1;
            });
        });
        $('#refTabel').on('click', '.deleteUser', function () {

var Customer_id = $(this).data("id");
Swal.fire({
      icon: 'question',
      title: 'Apakah akan menghapus Data Formulir ?',
      showCancelButton: true,
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya',
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
      $.ajax({
          type: "DELETE",
          url: "hapusformulir"+'/'+Customer_id,
          success: function (data) {
                table.ajax.url('listformulir').load();
              Swal.fire(data.success, '', 'success')
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });

  } else if (result.isDenied) {
      Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
  }
})


});
    });

   

</script>
<script>
@endpush
