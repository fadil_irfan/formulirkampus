@extends('layouts.main')

@section('title', 'Formulir')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Edit Formulir</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            Nama Pegawai : {{ $data->nama  }} ({{ $data->statuspegawai }})
          </div>
          <div class="card-body">
            <form method="POST" id="UploadSyaratForm" name="UploadSyaratForm">

              <input class="form-control" type="hidden" value="{{ $data->id }}" name="id">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-email">NIK (Nomor Induk Kependudukan)</label>
                <div class="col-sm-10">
                  <div class="input-group input-group-merge">
                    <input
                      type="text"
                      value="{{ $data->nik }}"
                      class="form-control"
                      placeholder="NIK (Nomor Induk Kependudukan)"
                      name="nik"
                       />
                    
                  </div>
                 
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-email">Nama Lengkap (Sesuai KTP)</label>
                <div class="col-sm-10">
                  <div class="input-group input-group-merge">
                    <input
                      type="text"
                      value="{{ $data->nama }}"
                      class="form-control"
                      placeholder="Nama Lengkap (Sesuai KTP)"
                      name="nama"
                       />
                    
                  </div>
                 
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Jenis Kelamin</label>
                <div class="col-sm-4">
                  <div class="row">
                    <div class="col-sm-6">
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="jk" id="membershipRadios1" value="L" {{ $data->jk == 'L' ? 'checked':'' }} >
                              Laki-Laki
                            <i class="input-helper"></i></label>
                          </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="jk" id="membershipRadios1" value="P" {{ $data->jk == 'P' ? 'checked':'' }} >
                              Perempuan
                            <i class="input-helper"></i></label>
                          </div>
                    </div>

                </div>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Status Pernikahan</label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Belum Menikah" {{ $data->status == 'Belum Menikah' ? 'checked':'' }} >
                              Belum Menikah
                            <i class="input-helper"></i></label>
                          </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Menikah" {{ $data->status == 'Menikah' ? 'checked':'' }} >
                              Menikah
                            <i class="input-helper"></i></label>
                          </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-check">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Cerai" {{ $data->status == 'Cerai' ? 'checked':'' }} >
                            Cerai
                          <i class="input-helper"></i></label>
                        </div>
                  </div>

                </div>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Jumlah Anak</label>
                <div class="col-sm-10">
                  <input class="form-control" value="{{ $data->jumlahanak }}" type="number" placeholder="Jumlah Anak" name="jumlahanak">
                </div>
              </div>
              
              
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Unit Kerja</label>
                <div class="col-sm-10">
                  <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="unitkerja">
                    <option value="">-- Pilih Unit Kerja --</option>
                    @foreach ($unitkerja as $l)
                              <option value="{{ $l->kode_fakultas }}" {{ $l->kode_fakultas==$data->unitkerja ? 'selected' : '' }} >
                                  {{ $l->nama_fakultas }}
                              </option>
                      @endforeach
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Status Pegawai</label>
                <div class="col-sm-10">
                  <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="statuspegawai">
                    <option value="">-- Pilih Status Pegawai --</option>
                    <option value="Tenaga Kependidikan" {{ $data->statuspegawai=="Tenaga Kependidikan" ? 'selected' : '' }}>Tenaga Kependidikan</option>
                    <option value="Dosen Tetap" {{ $data->statuspegawai=="Dosen Tetap" ? 'selected' : '' }}>Dosen Tetap Yayasan</option>
                    <option value="Dosen Tetap DPK" {{ $data->statuspegawai=="Dosen Tetap DPK" ? 'selected' : '' }}>Dosen Tetap DPK</option>
                    <option value="Dosen Luar Biasa" {{ $data->statuspegawai=="Dosen Luar Biasa" ? 'selected' : '' }}>Dosen Luar Biasa</option>
                  </select>
                  <label class="col-sm-6 col-form-label" for="basic-default-name">Unit Kerja Pilih Rektorat Untuk Dosen Luar Biasa</label>
                </div>
              </div>
              
              
              

              
              
              
              
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" id="saveSyarat" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->
  </div>
  <!-- / Content -->

  
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });

  

$('#UploadSyaratForm').submit(function (e) {
        e.preventDefault();
        $('#saveSyarat').html('Mengirim Data ..');
      
        $.ajax({
          data: new FormData(this),
          url: "updateformulir",
          method: "POST",
          dataType: 'json',
          contentType:false,
          cache:false,
          processData:false,
          success: function (data) {
              $('#UploadSyaratForm').trigger("reset");
              Swal.fire(
                  data.success,
                  'Update Berhasil ..',
                  'success'
              ).then(function (result) {
              if (result.value) {
                window.location.href = "{{URL::to('/listformulir')}}";
              }
          })
              
              $('#saveSyarat').html('Simpan');
      
          },
          error: function (data) {
              //console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#saveSyarat').html('Simpan');
          }
      });
    });

 


  });


</script>
@endpush
