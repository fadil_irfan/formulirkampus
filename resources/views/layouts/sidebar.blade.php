 <!-- Menu -->

 <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
      <a href="#" class="app-brand-link">
        <span class="app-brand-logo demo">
          
            
        </span>
        <span class="app-brand-text demo menu-text fw-bold ms-2">YPSA</span>
      </a>

      <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
        <i class="bx bx-chevron-left bx-sm align-middle"></i>
      </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
      <!-- Dashboards -->
      
      <li class="menu-item {{ (request()->segment(1) == 'dashboard') ? 'active' : '' }}">
        <a
          href="{{ url('dashboard') }}"
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-home-circle"></i>
          <div data-i18n="Email">Dashboard</div>
         
        </a>
      </li>
      {{-- <li class="menu-item {{ (request()->segment(1) == 'list') ? 'active open' : '' }}">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
          <i class="menu-icon tf-icons bx bx-universal-access"></i>
          <div data-i18n="Dashboards">Master Data</div>
          
        </a>
        <ul class="menu-sub">
          <li class="menu-item {{ (request()->segment(1) == 'list') ? 'active' : '' }}">
            <a
              href="{{ url('list') }}"
              class="menu-link">
              <div data-i18n="CRM">Data Pengguna</div>
              
            </a>
          </li>
          {{-- <li class="menu-item">
            <a href="index.html" class="menu-link">
              <div data-i18n="Analytics">Data Dokter</div>
            </a>
          </li>
          <li class="menu-item">
            <a
              href="https://demos.themeselection.com/sneat-bootstrap-html-admin-template/html/vertical-menu-template/app-ecommerce-dashboard.html"
              target="_blank"
              class="menu-link">
              <div data-i18n="eCommerce">Perawat</div>
              
            </a>
          </li>
        </ul>
      </li> --}}
      <li class="menu-item {{ (request()->segment(1) == 'listformulir') || (request()->segment(1) == 'form') ? 'active' : '' }}">
        <a
        href="{{ url('listformulir') }}"
        
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-male-female"></i>
          <div data-i18n="Email">Formulir</div>
         
        </a>
      </li>
      

      <li class="menu-item">
        <a
          href="{{ url('/') }}"
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-log-out"></i>
          <div data-i18n="Email">Keluar</div>
         
        </a>
      </li>

     
      

      

     
    </ul>
  </aside>
  <!-- / Menu -->