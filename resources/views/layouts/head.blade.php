<meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title') - {{ config('app.name') }}</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" 
     href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet" />

    <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="../assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <link rel="stylesheet" href="../assets/vendor/libs/apex-charts/apex-charts.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


    <style>
        /* Toastr */
.toast-success {
    background-color: #d813c8 !important;
}
.toast-warning {
    background-color: #357e45 !important;
}
.toast-error {
    background-color: #3533a7 !important;
}
      .expandable-table thead tr th {
    background: #4b49ac;
    padding: 10px;
    color: #ffffff;
    font-size: 14px;
}

.expandable-table thead tr th:first-child {
    border-radius: 8px 0 0 8px;
}

.expandable-table thead tr th:last-child {
    border-radius: 0 8px 8px 0;
}

.expandable-table tr.odd,
.expandable-table tr.even {
    box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.03);
    border-radius: 4px;
}

.expandable-table tr td {
    padding: 14px;
    font-size: 14px;
}

.expandable-table tr td.select-checkbox {
    padding-left: 26px;
}

.expandable-table tr td.select-checkbox:after {
    top: 2rem;
}

.expandable-table tr td.select-checkbox:before {
    top: 2rem;
}

.expandable-table tr td .cell-hilighted {
    background-color: #4b49ac;
    border-radius: 10px;
    padding: 18px;
    color: #fff;
    font-size: 11px;
}

.expandable-table tr td .cell-hilighted h5 {
    font-size: 20px;
    color: #52c4ff;
}

.expandable-table tr td .cell-hilighted p {
    opacity: 0.6;
    margin-bottom: 0;
}

.expandable-table tr td .cell-hilighted h6 {
    font-size: 14px;
    color: #52c4ff;
}

.expandable-table tr td .expanded-table-normal-cell {
    padding: 10px;
}

.expandable-table tr td .expanded-table-normal-cell p {
    font-size: 11px;
    margin-bottom: 0;
}

.expandable-table tr td .expanded-table-normal-cell h6 {
    color: #0b0f32;
    font-size: 14px;
}

.expandable-table tr td .expanded-table-normal-cell .highlighted-alpha {
    width: 34px;
    height: 34px;
    border-radius: 100%;
    background: #fe5c83;
    color: #ffffff;
    text-align: center;
    padding-top: 7px;
    font-size: 14px;
    margin-right: 8px;
}

.expandable-table tr td .expanded-table-normal-cell img {
    width: 34px;
    height: 34px;
    border-radius: 100%;
    margin-right: 8px;
}

.expandable-table tr td.details-control:before {
    content: "\e64b";
    font-family: "themify";
}

.expandable-table tr.shown td.details-control:before {
    content: "\e648";
}

.expandable-table .expanded-row {
    background: #fafafa;
}

table.dataTable tbody td.select-checkbox:before {
    top: 1.4rem;
    left: 10px;
    border: 1px solid #ced4da;
    width: 14px;
    height: 14px;
}

table.dataTable tbody td.select-checkbox:after {
    top: 1.5rem;
    left: 10px;
}

      
    </style>

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="../assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="../assets/js/config.js"></script>