<!DOCTYPE html>

<html
  lang="en"
  class="light-style layout-wide customizer-hide"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Formulir YPSA Sumedang</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet" />

    <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="../assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="../assets/vendor/css/pages/page-auth.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" 
       href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Helpers -->
    <script src="../assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="../assets/js/config.js"></script>
  </head>

  <body>
    <!-- Content -->

    <div class="container-xxl">
      <div class="container-xxl flex-grow-1 container-p-y">
        <div class="authentication-inner">
          <!-- Register -->
          <div class="card">
            <div class="card-body">
              <!-- Logo -->
              
              <!-- /Logo -->
              <h4 class="mb-2">Pendataan NPWP dan KK</h4>
              <p class="mb-4">Silahkan Lengkapi Formulir Berikut .. </p>

              <form method="POST" id="UploadSyaratForm" name="UploadSyaratForm" enctype="multipart/form-data">
                @csrf
                
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">NIK (Nomor Induk Kependudukan)</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input
                        type="text"
                        
                        class="form-control"
                        placeholder="NIK (Nomor Induk Kependudukan)"
                        name="nik"
                         />
                      
                    </div>
                   
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">Nama Lengkap (Sesuai KTP)</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="Nama Lengkap (Sesuai KTP)"
                        name="nama"
                         />
                      
                    </div>
                   
                  </div>
                </div>

                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Jenis Kelamin</label>
                  <div class="col-sm-4">
                    <div class="row">
                      <div class="col-sm-6">
                          <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jk" id="membershipRadios1" value="L" >
                                Laki-Laki
                              <i class="input-helper"></i></label>
                            </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jk" id="membershipRadios1" value="P" >
                                Perempuan
                              <i class="input-helper"></i></label>
                            </div>
                      </div>

                  </div>
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Status Pernikahan</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-4">
                          <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Belum Menikah" >
                                Belum Menikah
                              <i class="input-helper"></i></label>
                            </div>
                      </div>
                      <div class="col-sm-4">
                          <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Menikah" >
                                Menikah
                              <i class="input-helper"></i></label>
                            </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-check">
                            <label class="form-check-label">
                              <input type="radio" class="form-check-input" name="status" id="pernikahan" value="Cerai" >
                              Cerai
                            <i class="input-helper"></i></label>
                          </div>
                    </div>

                  </div>
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Jumlah Anak</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="number" placeholder="Jumlah Anak" name="jumlahanak">
                  </div>
                </div>
                
                
                
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Unit Kerja</label>
                  <div class="col-sm-10">
                    <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="unitkerja">
                      <option value="">-- Pilih Unit Kerja --</option>
                      @foreach ($unitkerja as $l)
                                <option value="{{ $l->kode_fakultas }}" >
                                    {{ $l->nama_fakultas }}
                                </option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-name">Status Pegawai</label>
                  <div class="col-sm-10">
                    <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="statuspegawai">
                      <option value="">-- Pilih Status Pegawai --</option>
                      <option value="Tenaga Kependidikan">Tenaga Kependidikan</option>
                      <option value="Dosen Tetap">Dosen Tetap Yayasan</option>
                      <option value="Dosen Tetap DPK">Dosen Tetap DPK</option>
                      <option value="Dosen Luar Biasa">Dosen Luar Biasa</option>
                    </select>
                    <label class="col-sm-6 col-form-label" for="basic-default-name">Unit Kerja Pilih Rektorat Untuk Dosen Luar Biasa</label>
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">Upload NPWP(Format *.jpeg/*.png) Jika Ada</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input class="form-control" type="file" id="formFile" name="pathNpwp" >
                      
                    </div>
                   
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">Upload KTP(Format *.jpeg/*.png)</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input class="form-control" type="file" id="formFile" name="pathKtp" >
                      
                    </div>
                   
                  </div>
                </div>
                <div class="row mb-3">
                  <label class="col-sm-2 col-form-label" for="basic-default-email">Upload Kartu Keluarga(Format *.jpeg/*.png)</label>
                  <div class="col-sm-10">
                    <div class="input-group input-group-merge">
                      <input class="form-control" type="file" id="formFile" name="pathKk" >
                      
                    </div>
                   
                  </div>
                </div>
                <div class="row justify-content-end">
                  <div class="col-sm-10">
                    <button type="submit" id="saveSyarat" class="btn btn-primary">Simpan</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
          <!-- /Register -->
        </div>
      </div>
    </div>

    

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->

    <script src="../assets/vendor/libs/jquery/jquery.js"></script>
    <script src="../assets/vendor/libs/popper/popper.js"></script>
    <script src="../assets/vendor/js/bootstrap.js"></script>
    <script src="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/js/menu.js"></script>
    
    @include('helpers.toast')

    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="../assets/js/main.js"></script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });

$('#UploadSyaratForm').submit(function (e) {
        e.preventDefault();
        $('#saveSyarat').html('Mengirim Data ..');
        document.getElementById("saveSyarat").disabled = true;
      
        $.ajax({
          data: new FormData(this),
          url: "simpanformulir",
          method: "POST",
          dataType: 'json',
          contentType:false,
          cache:false,
          processData:false,
          success: function (data) {
            if (data.status=='200')
            {
                $('#UploadSyaratForm').trigger("reset");
              Swal.fire(
                  'Simpan Data Berhasil',
                  'Silahkan klik tombol OK ' + data.success,
                  'success'
              ).then(function (result) {
              if (result.value) {
                window.location.href = "{{URL::to('/selesai')}}"
              }
              $('#saveSyarat').html('Simpan');
              document.getElementById("saveSyarat").disabled = false;
          })
            }else
            {
                Swal.fire(
                  'Terdapat Kesalahan',
                  data.error,
                  'error'
              )
            }
            $('#saveSyarat').html('Simpan');
            document.getElementById("saveSyarat").disabled = false;
              
      
          },
          error: function (data) {
              //console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#saveSyarat').html('Simpan');
              document.getElementById("saveSyarat").disabled = false;
          }
      });
    });

 


  });
</script>
  </body>
</html>
